package com.example;
import java.lang.Math;

public class LogicTraining {

	static boolean kidsRide(int temp,boolean isWinter) {
		if(isWinter) return (temp>=10 && temp<=30);
		else return (temp>=15 && temp<=30);
	}
	static boolean find8(int a,int b) {
		if(a==8 || b==8) return true;
		else if (a+b==8 || a*b==8 || Math.abs(b-a)==8) return true;
		else return false;
	}
	static int countSum(int a,int b,int c) {
		if(a!=b && b!=c) return a+b+c;
		else if(a==b && b!=c) return a+c;
		else if(a==c && c!=b) return a+b;
		else if(b==c && c!=a) return b+a;
		else return a;
	}
	static boolean goOut(boolean sundays,boolean withMom,boolean afterEight) {
		if(afterEight==true) return false;
		else if(sundays==true && withMom==true && afterEight==false)return true;
		else if(sundays==true && withMom==false) return false;
		return true;
	}
	
	static String canParty(int day,boolean isVacation) {
		if(isVacation==false) {
			if((day>=1 && day<=4)|| day==7) return "Dont party";
			else return "Party till midnight";
		}
		else {
			if((day>=1 && day<=4)|| day==7) return "Party till midnight";
			else return "Party all midnight";
		}
	}
	
	static boolean hangPaintings(int small, int big, int length) {
		int maxBig = length / 5;
		if (maxBig <= big) {
			length -= maxBig * 5;
		} else {
			length -= big * 5;
		}
		if (length <= small) {
			return true;
		}
		return false;
	}
}
