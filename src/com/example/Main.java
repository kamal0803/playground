package com.example;

import java.util.Arrays;

public class Main {
	
	public static void main(String[] args) {
		
		// doubleFirstHalf working correctly
		System.out.println(StringTraining.DoubleFirstHalf("HeyYou"));
		
		// interlace working correctly 
		System.out.println(StringTraining.interlace("A", "B"));
		
		//cutEnds working correctly
		System.out.println(StringTraining.cutEnds("Java"));
		
		// copyLastChars working correctly
		System.out.println(StringTraining.copyLastChars("computer"));
		
	}
}
