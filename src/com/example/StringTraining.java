package com.example;

public class StringTraining {
	
	static String goodJob(String name) {
		return "Good job, " + name + "!";
	}
	
	static String DoubleFirstHalf(String s1) {
		int len=s1.length();
		String str1;
		if(len%2==0) {
			str1=s1.substring(0,len/2);
			return str1+str1;
		}
		return s1;
	}
	
	static String interlace(String s1,String s2) {
		return s1+s2+s1+s2+s1;
	}
	
	static String cutEnds(String s1) {
		String str1="";
		int len=s1.length();
		if(len<=2) {
			return str1;
		}
		else {
			return s1.substring(1,len-1);
		}
	
	}
	
	static String copyLastChars(String s1) {
		int len=s1.length();
		if(len<=3) {
			return s1+" "+s1;
		}
		else {
			return s1.substring(len-3,len)+" "+s1.substring(len-3,len);
		}
	
	}
}
